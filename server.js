var cheerio = require('cheerio');
var request = require('request');
 
var i = 97 + 25;
var letter = String.fromCharCode(i);
var url = 'http://www.baseball-reference.com/players/{0}/'
	.replace("{0}", letter);
	
request(url, function(err, resp, body) {

	var $ = cheerio.load(body);
	$("blockquote a")
		.each(function(index, bball) {
			var $bball = $(bball);
			var name = $bball.text();
			console.log({ index: index, name: name, phone: '202.xxx.yyyy'});
		});
});
